import 'package:carousel_slider/carousel_slider.dart';
import 'package:final_project1/Northeast.dart';
import 'package:final_project1/central.dart';
import 'package:final_project1/easternregion.dart';
import 'package:final_project1/profile.dart';
import 'package:final_project1/questione.dart';
import 'package:final_project1/south.dart';
import 'package:final_project1/north.dart';
import 'package:final_project1/westernregion.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: MyHome(),
    );
  }
}

class MyHome extends StatefulWidget {
  const MyHome({Key? key}) : super(key: key);

  @override
  _MyHomeState createState() => _MyHomeState();
}

class _MyHomeState extends State<MyHome> {
  String name = '';
  String gender = 'M'; // M,F
  int age = 0;
  var questionScore = 0.0;

  @override
  void initState() {
    super.initState();
    _loadProfile();
    _loadQuestion();
  }

  Future<void> _loadQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    var strQuestionValue = prefs.getString('question_values') ??
        '[false, false, false, false, false, false, false, false, false, false, false]';
    print(strQuestionValue.substring(1, strQuestionValue.length - 1));
    var arrStrQuestionValues =
        strQuestionValue.substring(1, strQuestionValue.length - 1).split(',');
    setState(() {
      questionScore = 0.0;
      for (var i = 0; i < arrStrQuestionValues.length; i++) {
        if (arrStrQuestionValues[i].trim() == 'true') {
          questionScore = questionScore + 1;
        }
      }
      questionScore = (questionScore * 100) / 11;
    });
  }

  Future<void> _loadProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString('name') ?? '';
      gender = prefs.getString('gender') ?? 'M';
      age = prefs.getInt('age') ?? 0;
    });
  }

  Future<void> _resetProfile() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('name');
    prefs.remove('gender');
    prefs.remove('age');
    prefs.remove('question_values');
    await _loadProfile();
    await _loadQuestion();
  }

  final List<String> imageList = [
    'assets/cen6.jpg',
    'https://upload.wikimedia.org/wikipedia/commons/b/ba/%E0%B8%A7%E0%B8%B1%E0%B8%94%E0%B8%A1%E0%B8%AB%E0%B8%B2%E0%B8%98%E0%B8%B2%E0%B8%95%E0%B8%B8%E0%B8%A7%E0%B8%A3%E0%B8%A7%E0%B8%B4%E0%B8%AB%E0%B8%B2%E0%B8%A3_%28%E0%B8%88%E0%B8%B1%E0%B8%87%E0%B8%AB%E0%B8%A7%E0%B8%B1%E0%B8%94%E0%B8%A3%E0%B8%B2%E0%B8%8A%E0%B8%9A%E0%B8%B8%E0%B8%A3%E0%B8%B5%29.jpg',
    'https://img.kapook.com/u/2019/pattra/pattraveljuly2019/travel263.jpg',
    'assets/chiangrai.jpg',
    'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
    'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
    'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('สถานที่ท่องเที่ยว')),
      body: Column(
        children: [
          ListTile(
            leading: Icon(Icons.person),
            title: Text(name),
            subtitle: Text('เพศ: $gender, อายุ: $age ปี',
                style: TextStyle(color: Colors.black.withOpacity(0.6))),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              'ท่องเที่ยว: ${questionScore.toStringAsFixed(2)} %',
              style: TextStyle(
                  fontWeight: (questionScore > 30)
                      ? FontWeight.bold
                      : FontWeight.normal,
                  fontSize: 24.0,
                  color: (questionScore > 30)
                      ? Colors.red.withOpacity(0.6)
                      : Colors.black.withOpacity(0.6)),
            ),
          ),
          Container(
            child: CarouselSlider(
              options: CarouselOptions(
                enlargeCenterPage: true,
                enableInfiniteScroll: false,
                autoPlay: true,
              ),
              items: imageList
                  .map((e) => ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: Stack(
                          fit: StackFit.expand,
                          children: <Widget>[
                            Image.network(
                              e,
                              width: 1050,
                              height: 350,
                              fit: BoxFit.cover,
                            )
                          ],
                        ),
                      ))
                  .toList(),
            ),
          ),
        ],
      ),
      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
              child: Text('เมนูเลือกภูมิภาค'),
            ),
            ListTile(
              title: Text('โปรไฟล์'),
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ProfileWidget()));
                await _loadProfile();
              },
            ),
            ListTile(
              title: const Text('ภาคเหนือ'),
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => North()));
              },
            ),
            ListTile(
              title: const Text('ภาคกลาง'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Central()));
              },
            ),
            ListTile(
              title: const Text('ภาคตะวันออกเฉียงเหนือ'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Northeast()));
              },
            ),
            ListTile(
              title: const Text('ภาคใต้'),
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => South()));
              },
            ),
            ListTile(
              title: const Text('ภาคตะวันออก'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Easternregion()));
              },
            ),
            ListTile(
              title: const Text('ภาคตะวันตก'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Westernregion()));
              },
            ),
            ListTile(
              title: Text('สำรวจ'),
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Questionwidget()));
                await _loadQuestion();
              },
            ),
          ],
        ),
      ),
    );
  }
}
