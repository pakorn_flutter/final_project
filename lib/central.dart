import 'dart:math';
import 'package:flutter/material.dart';

class Central extends StatefulWidget {
  @override
  _CentralState createState() => _CentralState();
}

class _CentralState extends State<Central> {
  List<String> places = [
    'จังหวัดกำแพงเพชร',
    'จังหวัดชัยนาท',
    'จังหวัดนครนายก',
    'จังหวัดนครปฐม',
    'จังหวัดนครสวรรค์',
    'จังหวัดนนทบุรี',
    'จังหวัดปทุมธานี ',
    'จังหวัดพระนครศรีอยุธยา ',
    'จังหวัดพิจิตร',
    'จังหวัดพิษณุโลก',
    'จังหวัดเพชรบูรณ์',
    'จังหวัดลพบุรี',
    'จังหวัดสมุทรปราการ ',
    'จังหวัดสมุทรสงคราม ',
    'จังหวัดสมุทรสาคร',
    'จังหวัดสิงห์บุรี',
    'จังหวัดสุโขทัย',
    'จังหวัดสุพรรณบุรี ',
    'จังหวัดสระบุรี',
    'จังหวัดอ่างทอง',
    'จังหวัดอุทัยธานี'
  ];
  TextEditingController placeController = TextEditingController();
  TextEditingController placerandom = TextEditingController();
  var placenew = '';
  @override
  void dispose() {
    placeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('ภาคกลาง'),
        ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                colorFilter: ColorFilter.mode(
                    Colors.black.withOpacity(0.5), BlendMode.dstATop),
                image: AssetImage('assets/cen1.jpg'),
                fit: BoxFit.cover),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                children: [
                  Container(
                      alignment: Alignment.center,
                      width: 300,
                      height: 300,
                      child: TextField(
                        controller: placerandom,
                        readOnly: true,
                        enableInteractiveSelection: false,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: '',
                        ),
                      ),
                      padding: EdgeInsets.all(40),
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.blueAccent,
                      )
                      //borderRadius: BorderRadius.circular(20)),
                      ),
                  Container(
                    padding: EdgeInsets.all(20),
                    width: 300,
                    height: 100,
                    // ignore: deprecated_member_use
                    child: RaisedButton(
                      onPressed: () {
                        var random = Random();
                        placenew = places[random.nextInt(places.length)];
                        placerandom.text = placenew;
                        print(placenew);
                      },
                      child: Text(
                        'กดเพื่อสุ่ม',
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                      color: Colors.black,
                    ),
                  )
                ],
              )
            ],
          ),
        ));
  }
}
