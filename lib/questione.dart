import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Questionwidget extends StatefulWidget {
  Questionwidget({Key? key}) : super(key: key);

  @override
  _QuestionwidgetState createState() => _QuestionwidgetState();
}

class _QuestionwidgetState extends State<Questionwidget> {
  var questionvalues = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];
  var questions = [
    'จังหวัดเชียงราย',
    'จังหวัดเชียงใหม่',
    'จังหวัดภูเก็ต',
    'จังหวัดชัยภูมิ',
    'จังหวัดแพร่',
    'จังหวัดแม่ฮ่องสอน',
    'จังหวัดลำปาง',
    'จังหวัดลำพูน',
    'จังหวัดจันทบุรี',
    'จังหวัดฉะเชิงเทรา',
    'จังหวัดกระบี่'
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('สำรวจ')),
      body: ListView(
        children: [
          Container(
            padding: EdgeInsets.all(10),
            child: Text('คุณเคยไปท่องเที่ยวจังหวัดไหนบ้าง',
                style: TextStyle(fontSize: 24)),
          ),
          CheckboxListTile(
              value: questionvalues[0],
              title: Text(questions[0]),
              onChanged: (newValue) {
                setState(() {
                  questionvalues[0] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionvalues[1],
              title: Text(questions[1]),
              onChanged: (newValue) {
                setState(() {
                  questionvalues[1] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionvalues[2],
              title: Text(questions[2]),
              onChanged: (newValue) {
                setState(() {
                  questionvalues[2] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionvalues[3],
              title: Text(questions[3]),
              onChanged: (newValue) {
                setState(() {
                  questionvalues[3] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionvalues[4],
              title: Text(questions[4]),
              onChanged: (newValue) {
                setState(() {
                  questionvalues[4] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionvalues[5],
              title: Text(questions[5]),
              onChanged: (newValue) {
                setState(() {
                  questionvalues[5] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionvalues[6],
              title: Text(questions[6]),
              onChanged: (newValue) {
                setState(() {
                  questionvalues[6] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionvalues[7],
              title: Text(questions[7]),
              onChanged: (newValue) {
                setState(() {
                  questionvalues[7] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionvalues[8],
              title: Text(questions[8]),
              onChanged: (newValue) {
                setState(() {
                  questionvalues[8] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionvalues[9],
              title: Text(questions[9]),
              onChanged: (newValue) {
                setState(() {
                  questionvalues[9] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionvalues[10],
              title: Text(questions[10]),
              onChanged: (newValue) {
                setState(() {
                  questionvalues[10] = newValue!;
                });
              }),
          ElevatedButton(
              onPressed: () async {
                await _saveQuestion();
                Navigator.pop(context);
              },
              child: const Text('Save'))
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadQuestion();
  }

  Future<void> _loadQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    var strQuestionValue = prefs.getString('question_values') ??
        "[false,false,false,false,false,false,false,false,false,false,false]";
    print(strQuestionValue.substring(1, strQuestionValue.length - 1));
    var arrStrQuestionValues =
        strQuestionValue.substring(1, strQuestionValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrQuestionValues.length; i++) {
        questionvalues[i] = (arrStrQuestionValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('question_values', questionvalues.toString());
  }
}
