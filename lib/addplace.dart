import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';

class AddPlace extends StatefulWidget {
  const AddPlace({Key? key}) : super(key: key);

  @override
  _AddPlaceState createState() => _AddPlaceState();
}

class _AddPlaceState extends State<AddPlace> {
  List<String> places = [];
  TextEditingController placeController = TextEditingController();
  TextEditingController placerandom = TextEditingController();
  var placenew = '';
  @override
  void dispose() {
    placeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('เพิ่มสถานที่'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
              child: Text(
            ' เพิ่มสถานที่ที่ชอบ',
            style: TextStyle(
                fontSize: 24, color: Colors.red, fontWeight: FontWeight.w600),
          )),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.all(20),
                  child: TextField(
                    controller: placeController,
                    decoration: InputDecoration(
                      fillColor: Color(0xFFDEB887),
                      border: OutlineInputBorder(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(20))),
                      labelText: 'พิมพ์ที่นี่',
                    ),
                    style: TextStyle(color: Colors.black),
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(left: 20, right: 20),
                    child: MaterialButton(
                      onPressed: () {
                        addToList();
                        print(places.toString());
                        clearText();
                      },
                      child:
                          Text('เพิ่ม', style: TextStyle(color: Colors.white)),
                      color: Colors.black,
                    ))
              ],
            ),
          ),
          Container(
              alignment: Alignment.center,
              width: 300,
              height: 300,
              child: TextField(
                controller: placerandom,
                readOnly: true,
                enableInteractiveSelection: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: '',
                ),
              ),
              padding: EdgeInsets.all(40),
              margin: EdgeInsets.all(10),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.blueAccent,
              )
              //borderRadius: BorderRadius.circular(20)),
              ),
          Container(
            // ignore: deprecated_member_use
            child: RaisedButton(
              onPressed: () {
                var random = Random();
                placenew = places[random.nextInt(places.length)];
                placerandom.text = placenew;
                print(placenew);
              },
              child: Text('กดเพื่อสุ่ม', style: TextStyle(color: Colors.white)),
              color: Colors.black,
            ),
          )
        ],
      ),
      //backgroundColor:,
    );
  }

  void addToList() {
    if (placeController.text.isNotEmpty) {
      setState(() {
        places.add(placeController.text);
      });
    }
  }

  void clearText() {
    placeController.clear();
  }
}
