import 'dart:math';
import 'package:flutter/material.dart';

class Easternregion extends StatefulWidget {
  const Easternregion({Key? key}) : super(key: key);

  @override
  _EasternregionState createState() => _EasternregionState();
}

class _EasternregionState extends State<Easternregion> {
  List<String> places = [
    'จังหวัดจันทบุรี',
    'จังหวัดฉะเชิงเทรา',
    'จังหวัดชลบุรี',
    'จังหวัดตราด',
    'จังหวัดปราจีนบุรี',
    'จังหวัดระยอง',
    'จังหวัดสระแก้ว'
  ];
  TextEditingController placeController = TextEditingController();
  TextEditingController placerandom = TextEditingController();
  var placenew = '';
  @override
  void dispose() {
    placeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('ภาคตะวันออก'),
        ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                colorFilter: ColorFilter.mode(
                    Colors.black.withOpacity(0.5), BlendMode.dstATop),
                image: AssetImage('assets/cen4.jpg'),
                fit: BoxFit.cover),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                children: [
                  Container(
                      alignment: Alignment.center,
                      width: 300,
                      height: 300,
                      child: TextField(
                        controller: placerandom,
                        readOnly: true,
                        enableInteractiveSelection: false,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: '',
                        ),
                      ),
                      padding: EdgeInsets.all(40),
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.blueAccent,
                      )
                      //borderRadius: BorderRadius.circular(20)),
                      ),
                  Container(
                    padding: EdgeInsets.all(20),
                    width: 300,
                    height: 100,
                    // ignore: deprecated_member_use
                    child: RaisedButton(
                      onPressed: () {
                        var random = Random();
                        placenew = places[random.nextInt(places.length)];
                        placerandom.text = placenew;
                        print(placenew);
                      },
                      child: Text(
                        'กดเพื่อสุ่ม',
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                      color: Colors.black,
                    ),
                  )
                ],
              )
            ],
          ),
        ));
  }
}
